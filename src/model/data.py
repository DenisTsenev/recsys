import click
from typing import Tuple
from sqlalchemy.orm import Session
import pandas as pd

from ..app.database import SessionLocal
from ..app.table_post import Prepared_Data, User, Feed_Data


def get_db() -> Session:
    with SessionLocal() as db:
        return db


def get_dataset(
    db: Session = get_db(),
) -> Tuple[pd.DataFrame, pd.Series]:
    click.echo("Data take")
    prepared_data = pd.read_sql(db.query(Prepared_Data).statement, db.bind)
    click.echo("prepared_data take")
    user_info = pd.read_sql(db.query(User).statement, db.bind)
    click.echo("user_info take")
    feed_data = pd.read_sql(db.query(Feed_Data).limit(1000).statement, db.bind)
    click.echo("feed_data take")
    data = pd.merge(
        pd.merge(feed_data, prepared_data, on="post_id", how="left"),
        user_info,
        on="user_id",
        how="left",
    )
    data = data[data.age < 50]
    data["hour"] = pd.to_datetime(data["timestamp"]).apply(lambda x: x.hour)
    data[["TextCluster", "gender", "exp_group", "hour"]] = data[
        ["TextCluster", "gender", "exp_group", "hour"]
    ].astype("object")
    data = data.drop(["action", "text", "timestamp"], axis=1)
    data = data.groupby(["user_id", "post_id"]).agg("max")
    features = data.drop(["target"], axis=1)
    target = data["target"]
    click.echo("Data was taken")
    return features, target
