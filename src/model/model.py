import numpy as np
import pandas as pd
from typing import Any, Tuple
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
import warnings
warnings.simplefilter(action="ignore", category=FutureWarning)
from category_encoders import TargetEncoder
from category_encoders.one_hot import OneHotEncoder
from sklearn.model_selection import TimeSeriesSplit, GridSearchCV, cross_validate
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from catboost import CatBoostClassifier


def get_model(model_name: str, random_state: int) -> Any:
    if model_name == "CatBoost":
        return CatBoostClassifier(
            eval_metric="AUC",
            early_stopping_rounds=20,
            verbose=50,
            random_seed=random_state,
        )
    if model_name == "LogReg":
        return LogisticRegression(random_state=random_state)
    if model_name == "knn":
        return KNeighborsClassifier(n_jobs=-1)
    if model_name == "RandForest":
        return RandomForestClassifier(random_state=random_state, n_jobs=-1)


def create_pipeline(
    features: pd.DataFrame, model_name: str, random_state: int
) -> Pipeline:
    pipeline_steps = []
    ohe_inx, mte_inx = get_columns(features)
    pipeline_steps.append(
        (
            "Encoder",
            ColumnTransformer(
                transformers=[
                    ("ohe", OneHotEncoder(), ohe_inx),
                    ("mte", TargetEncoder(), mte_inx),
                ],
                remainder="passthrough",
            ),
        )
    )
    pipeline_steps.append(("StandartScaler", StandardScaler()))
    pipeline_steps.append((f"{model_name}", get_model(model_name, random_state)))
    return Pipeline(steps=pipeline_steps)


def get_columns(features: pd.DataFrame) -> list:
    object_cols = list(features.select_dtypes(include="object").columns)
    # Columns for encoding
    cols_for_ohe = [x for x in object_cols if features[x].nunique() < 5]
    cols_for_mte = [x for x in object_cols if features[x].nunique() >= 5]
    # Same index columns
    cols_for_ohe_idx = [list(features.columns).index(col) for col in cols_for_ohe]
    cols_for_mte_idx = [list(features.columns).index(col) for col in cols_for_mte]
    return cols_for_ohe_idx, cols_for_mte_idx


def get_param_grid(model_name: str) -> dict:
    param_grid = dict()
    if model_name == "CatBoost":
        param_grid[f"{model_name}__iterations"] = [30]
        param_grid[f"{model_name}__learning_rate"] = [0.6]
        param_grid[f"{model_name}__depth"] = [4]
        param_grid[f"{model_name}__l2_leaf_reg"] = [3]
    if model_name == "LogReg":
        param_grid[f"{model_name}__penalty"] = ["l2"]
        param_grid[f"{model_name}__C"] = np.logspace(-4, 4, 4)
        param_grid[f"{model_name}__solver"] = ["newton-cg", "lbfgs", "saga"]
        param_grid[f"{model_name}__max_iter"] = [1000, 2000, 3000, 4000, 5000]
    if model_name == "knn":
        param_grid[f"{model_name}__n_neighbors"] = [3, 5, 11, 19]
        param_grid[f"{model_name}__weights"] = ["uniform", "distance"]
        param_grid[f"{model_name}__metric"] = ["euclidean", "manhattan", "minkowski"]
    if model_name == "RandForest":
        param_grid[f"{model_name}__n_estimators"] = [10]
        param_grid[f"{model_name}__max_depth"] = [5, 7]
        param_grid[f"{model_name}__max_features"] = [None]
    return param_grid


def train_model(
    model_name: str,
    features: pd.DataFrame,
    target: pd.Series,
    random_state: int,
    scoring: str,
) -> Tuple[float, float, float]:
    tscv = TimeSeriesSplit(n_splits=10)
    model = create_pipeline(features, model_name, random_state)
    param_grid = get_param_grid(model_name)
    search = GridSearchCV(model, param_grid, scoring=scoring, cv=tscv, refit=True)
    scores = cross_validate(
        search,
        features,
        target,
        scoring=["roc_auc", "average_precision"],
        cv=tscv,
        n_jobs=-1,
        return_train_score=True,
    )
    return (
        np.mean(scores["train_roc_auc"]),
        np.mean(scores["test_roc_auc"]),
        np.mean(scores["train_average_precision"]),
        np.mean(scores["test_average_precision"]),
    )


def get_tuned_model(
    model_name: str,
    features: pd.DataFrame,
    target: pd.Series,
    random_state: int,
    scoring: str,
) -> Any:
    tscv = TimeSeriesSplit(n_splits=10)
    model = create_pipeline(features, model_name, random_state)
    param_grid = get_param_grid(model_name)
    search = GridSearchCV(model, param_grid, scoring=scoring, cv=tscv, refit=True)
    search.fit(features, target)
    return search.best_estimator_
