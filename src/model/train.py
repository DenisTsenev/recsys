import os
import click
import mlflow
from mlflow.models.signature import infer_signature
from pathlib import Path
from typing import Any
from dotenv import load_dotenv

from .data import get_dataset
from .model import get_tuned_model
from .model import train_model

# Load the enviroment variables from the .env file into the application
load_dotenv()

remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)


@click.command()
@click.option(
    "--model-name",
    default="LogReg",
    type=click.Choice(["LogReg", "CatBoost", "knn", "RandForest"]),
    help="Model type for evaluation",
    show_default=True,
)
@click.option(
    "--random-state", default=42, type=int, help="Random state", show_default=True
)
@click.option(
    "--scoring", default="roc_auc", type=str, help="Flag to score", show_default=True
)
def train(model_name: str, random_state: int, scoring: str) -> None:
    mlflow.set_experiment(model_name)
    click.echo("Training started")

    features, target = get_dataset()

    with mlflow.start_run():

        train_roc_auc, train_pr_auc, test_roc_auc, test_pr_auc = train_model(
            model_name, features, target, random_state, scoring
        )

        # mlflow log metrics and params
        log_metrics(train_roc_auc, train_pr_auc, test_roc_auc, test_pr_auc)
        log_params(model_name)

        click.echo(f"train_roc_auc: {train_roc_auc}")
        click.echo(f"train_pr_auc: {train_pr_auc}")
        click.echo(f"test_roc_auc: {test_roc_auc}")
        click.echo(f"test_pr_auc: {test_pr_auc}")

        model = get_tuned_model(model_name, features, target, random_state, scoring)

        signature = infer_signature(features, model.predict(features))
        log_models(model, model_name, signature=signature)


def log_metrics(
    train_roc_auc: float, train_pr_auc: float, test_roc_auc: float, test_pr_auc: float
) -> None:
    mlflow.log_metric("train_roc_auc", train_roc_auc)
    mlflow.log_metric("test_roc_auc", test_roc_auc)
    mlflow.log_metric("train_pr_auc", train_pr_auc)
    mlflow.log_metric("test_pr_auc", test_pr_auc)


def log_params(model_name: str) -> None:
    mlflow.log_param("model_name", model_name)


def log_models(model: Any, model_name: str, signature: Any) -> None:
    mlflow.sklearn.log_model(
        model,
        artifact_path=model_name,
        registered_model_name=model_name,
        signature=signature,
    )

if __name__ == "__model__":
    train()