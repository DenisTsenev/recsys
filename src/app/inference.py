import os
import pandas as pd
import mlflow
from typing import List
from fastapi import FastAPI
from loguru import logger
from datetime import datetime
from sqlalchemy.orm import Session
from dotenv import load_dotenv

from schema import PostGet
from table_post import User, Post, Feed_Data, Prepared_Data
from database import SessionLocal

app = FastAPI()

# Load the enviroment variables from the .env file into the application
load_dotenv()
os.environ["MLFLOW_S3_ENDPOINT_URL"] = os.getenv("MLFLOW_S3_ENDPOINT_URL")


def get_db() -> Session:
    with SessionLocal() as db:
        return db


def batch_load_sql(query, con):
    chunks = []
    for chunk_df in pd.read_sql(query, con, chunksize=10000):
        chunks.append(chunk_df)
    return pd.concat(chunks, ignore_index=True)


def load_features(db: Session = get_db()):
    # Unique post_id and user_id
    liked_post_query = (
        db.query(Feed_Data)
        .distinct(Feed_Data.user_id, Feed_Data.post_id)
        .filter(Feed_Data.action == "like")
        .statement
    )
    liked_post = batch_load_sql(liked_post_query, db.bind)

    # Features based on Tf-Idf
    logger.info("Loading post features")
    post_features = pd.read_sql(db.query(Prepared_Data).statement, db.bind)

    # Features on user_id
    logger.info("Loading user features")
    user_features = pd.read_sql(
        db.query(User).statement,
        db.bind,
    )
    return [liked_post, post_features, user_features]


logger.info("loading model")
model = mlflow.sklearn.load_model("models:/CatBoost/Staging")
logger.info("loading features")
features = load_features()
logger.info("service is up and running")


def get_recommended_feed(id: int, time: datetime, limit: int):
    # load user features
    logger.info(f"user id: {id}")
    logger.info("reading features")
    user_features = features[2].loc[features[2].user_id == id]
    user_features = user_features.drop("user_id", axis=1)

    # drop columns
    logger.info("dropping columns")
    post_features = features[1].drop(["text"], axis=1)
    content = features[1][["post_id", "text", "topic"]]

    # concat features
    logger.info("zipping features")
    add_user_features = dict(zip(user_features.columns, user_features.values[0]))
    logger.info("assigning features")
    user_post_features = post_features.assign(**add_user_features)
    user_post_features = user_post_features.set_index("post_id")

    # add an hour of recommendation
    logger.info("add time info")
    user_post_features["hour"] = time.hour

    # make predictions
    logger.info("predicting")
    predict = model.predict_proba(user_post_features)[:, 1]
    user_post_features["predicts"] = predict

    # filter post where "like" were posted
    logger.info("filter post")
    liked_post = features[0]
    liked_post = liked_post[liked_post.user_id == id].post_id.values
    filtered_ = user_post_features[~user_post_features.index.isin(liked_post)]

    # recommend post
    recommended_post = filtered_.sort_values("predicts")[-limit:].index

    return [
        PostGet(
            **{
                "id": i,
                "text": content[content.post_id == i].text.values[0],
                "topic": content[content.post_id == i].topic.values[0],
            }
        )
        for i in recommended_post
    ]


@app.get("/post/recommendations/", response_model=List[PostGet])
def recommendatded_post(id: int, time: datetime, limit: int = 10) -> List[PostGet]:
    return get_recommended_feed(id, time, limit)
