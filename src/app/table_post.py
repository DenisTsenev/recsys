from .database import Base

from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy import Float
from sqlalchemy.orm import relationship


class Post(Base):
    __tablename__ = "post_text_df"
    __table_args__ = {"schema": "public"}

    post_id = Column(Integer, primary_key=True)
    text = Column(String)
    topic = Column(String)


class User(Base):
    __tablename__ = "user_data"
    __table_args__ = {"schema": "public"}

    user_id = Column(Integer, primary_key=True)
    gender = Column(Integer)
    age = Column(Integer)
    country = Column(String)
    city = Column(String)
    exp_group = Column(Integer)
    os = Column(String)
    source = Column(String)


class Feed_Data(Base):
    __tablename__ = "feed_data"
    __table_args__ = {"schema": "public"}

    timestamp = Column(DateTime)
    user_id = Column(Integer, primary_key=User.user_id)
    relationship(User)
    post_id = Column(Integer, primary_key=Post.post_id)
    relationship(Post)
    action = Column(String)
    target = Column(Integer)


class Prepared_Data(Base):
    __tablename__ = "PreparedData"
    __table_args__ = {"schema": "public"}

    post_id = Column(Integer, primary_key=Post.post_id)
    relationship(Post)
    text = Column(String)
    topic = Column(String)
    TextCluster = Column(Integer)
    DistanceTo_1th_Cluster = Column(Float)
    DistanceTo_2th_Cluster = Column(Float)
    DistanceTo_3th_Cluster = Column(Float)
    DistanceTo_4th_Cluster = Column(Float)
    DistanceTo_5th_Cluster = Column(Float)
    DistanceTo_6th_Cluster = Column(Float)
    DistanceTo_7th_Cluster = Column(Float)
    DistanceTo_8th_Cluster = Column(Float)
    DistanceTo_9th_Cluster = Column(Float)
    DistanceTo_10th_Cluster = Column(Float)
    DistanceTo_11th_Cluster = Column(Float)
    DistanceTo_12th_Cluster = Column(Float)
    DistanceTo_13th_Cluster = Column(Float)
    DistanceTo_14th_Cluster = Column(Float)
    DistanceTo_15th_Cluster = Column(Float)
