import inference
from fastapi.testclient import TestClient
from datetime import datetime

client = TestClient(inference.app)

user_id = 15444
time = datetime(2021, 12, 20)


try:
    r = client.get(
        f"/post/recommendations/", params={"id": user_id, "time": time, "limit": 5}
    )
except Exception as e:
    raise ValueError(f" Ошибка выполнения запроса {type(e)} {str(e)}")

print(r.json())
